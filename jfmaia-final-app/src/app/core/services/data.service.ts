import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import {
  ICustomer,
  IPagedResults,
  IApiResponse,
  IState
} from "../interfaces/interfaces";
import { map, catchError } from "rxjs/operators";

@Injectable()
export class DataService {
  private endpoint = environment.customersBaseUrl;

  constructor(private http: HttpClient) {}

  // Get list of customers according to pagination.
  getCustomersPage(
    page: number,
    pageSize: number
  ): Observable<IPagedResults<ICustomer[]>> {
    const url = `${this.endpoint}/page/${page}/${pageSize}`;
    return this.http.get<ICustomer[]>(url, { observe: "response" }).pipe(
      map(response => {
        const records = +response.headers.get("X-InlineCount");
        const customers = response.body as ICustomer[];
        return {
          results: customers,
          totalRecords: records
        };
      }),
      catchError(this.handleError)
    );
  }

  getCustomer(id: number): Observable<ICustomer> {
    const url = `${this.endpoint}/${id}/`;
    return this.http.get<ICustomer>(url).pipe(
      map(customer => {
        return customer;
      }),
      catchError(this.handleError)
    );
  }

  insertCustomer(customer: ICustomer): Observable<ICustomer> {
    return this.http
      .post<ICustomer>(this.endpoint, customer)
      .pipe(catchError(this.handleError));
  }

  updateCustomer(customer: ICustomer): Observable<boolean> {
    const url = `${this.endpoint}/${customer.id}/`;
    return this.http.put<IApiResponse>(url, customer).pipe(
      map(res => res.status),
      catchError(this.handleError)
    );
  }

  deleteCustomer(id: number): Observable<boolean> {
    const url = `${this.endpoint}/${id}/`;
    return this.http.delete<IApiResponse>(url).pipe(
      map(res => res.status),
      catchError(this.handleError)
    );
  }

  getStates(): Observable<IState[]> {
    return this.http
      .get<IState[]>("/api/states")
      .pipe(catchError(this.handleError));
  }

  // Method to handle server errors.
  private handleError(error: HttpErrorResponse) {
    console.error("server error:", error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || "Node.js server error");
  }
}
