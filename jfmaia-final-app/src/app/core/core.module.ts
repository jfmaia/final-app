import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NavbarComponent } from "./navbar/navbar.component";
import { ClarityModule } from "@clr/angular";
import { DataService } from "./services/data.service";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent, HttpClientModule],
  providers: [DataService],
  imports: [CommonModule, ClarityModule, HttpClientModule, RouterModule]
})
export class CoreModule {}
