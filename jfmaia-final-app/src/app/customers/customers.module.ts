import { NgModule } from "@angular/core";
import { CustomerCardComponent } from "./customer-card/customer-card.component";
import { CustomerComponent } from "./customers.component";
import { CustomersRoutingModule } from "./customers-routing.module";
import { CommonModule } from "@angular/common";
import { ClarityModule } from "@clr/angular";
import { CustomerEditComponent } from "./customer-edit/customer-edit.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatOptionModule
} from "@angular/material";

@NgModule({
  imports: [
    CustomersRoutingModule,
    CommonModule,
    ClarityModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule
  ],
  declarations: [
    CustomerCardComponent,
    CustomerComponent,
    CustomerEditComponent
  ]
})
export class CustomersModule {}
