import { Component, OnInit, OnChanges, SimpleChanges } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { DataService } from "src/app/core/services/data.service";
import { ICustomer, IState } from "src/app/core/interfaces/interfaces";

@Component({
  selector: "app-customer-edit",
  templateUrl: "./customer-edit.component.html",
  styleUrls: ["./customer-edit.component.scss"]
})
export class CustomerEditComponent implements OnInit {
  id: number;
  private sub: any;
  states: IState[];
  customer: ICustomer = {
    id: 0,
    firstName: "",
    lastName: "",
    gender: "",
    address: "",
    city: "",
    state: {
      abbreviation: "",
      name: ""
    }
  };

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private router: Router
  ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const parameter = "id";
      this.id = +params[parameter];
    });
    this.getCustomerDetails();
    this.dataService
      .getStates()
      .subscribe((states: IState[]) => (this.states = states));
  }

  getCustomerDetails() {
    this.dataService.getCustomer(this.id).subscribe(response => {
      this.customer = response;
    });
  }

  cancel(event: Event) {
    event.preventDefault();
    this.router.navigate(["/customers"]);
  }

  delete(event: Event) {
    event.preventDefault();
    this.dataService.deleteCustomer(this.id).subscribe(
      () => {
        this.router.navigate(["/customers"]);
      },
      () => {
        prompt(
          `It wasn\'t possible to delete ${this.customer.firstName} ${this.customer.lastName}`
        );
      }
    );
  }

  update(event: Event) {
    event.preventDefault();
    this.dataService.updateCustomer(this.customer).subscribe(
      () => {
        this.router.navigate(["/customers"]);
      },
      () => {
        prompt(
          `It wasn\'t possible to update ${this.customer.firstName} ${this.customer.lastName} data`
        );
      }
    );
  }
}
