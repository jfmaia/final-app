import { Component, OnInit } from "@angular/core";
import { DataService } from "../core/services/data.service";
import { IPagedResults, ICustomer } from "../core/interfaces/interfaces";

@Component({
  selector: "app-customer",
  templateUrl: "./customers.component.html"
})
export class CustomerComponent implements OnInit {
  customers: ICustomer[];
  view = "card";
  page: number;
  pageSize = 10;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.page = 0;
    this.getCustomers();
  }

  changeView(view: string) {
    this.view = view;
  }

  getCustomers() {
    this.dataService.getCustomersPage(this.page, this.pageSize).subscribe(
      (response: IPagedResults<ICustomer[]>) => {
        this.customers = response.results;
      },
      err => {
        console.error(err);
      }
    );
  }
}
