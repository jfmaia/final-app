import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CustomerComponent } from "./customers.component";
import { CustomerEditComponent } from "./customer-edit/customer-edit.component";

const routes: Routes = [
  { path: "", component: CustomerComponent },
  { path: "customers/:id/edit", component: CustomerEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {}
